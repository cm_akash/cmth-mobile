import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {}

  get(url: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.get(`${environment.apiUrl}${url}`, {headers: this.headers, params});
  }

  post(url: string, data: object = {}): Observable<any> {
    return this.http.post(`${environment.apiUrl}${url}`, JSON.stringify(data), { headers: this.headers });
  }

  put(url: string, data: object = {}): Observable<any> {
    return this.http.put(`${environment.apiUrl}${url}`, JSON.stringify(data), { headers: this.headers });
  }

  delete(url: string): Observable<any> {
    return this.http.delete(`${environment.apiUrl}${url}`, { headers: this.headers });
  }

  get headers(): HttpHeaders {
    const headersConfig = {
      'Content-Type': 'application/json',
      Accept: 'application/json'
    };

    return new HttpHeaders(headersConfig);
  }
}
