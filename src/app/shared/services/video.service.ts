import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import {HttpClient} from '@angular/common/http';
import {switchMap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VideoService {

  censoredCountries = ['Dubai', 'Kuwait', 'India'];
  censor = true; // if countries are adult content sensitive, then true.
  constructor(private apiService: ApiService,
              private httpService: HttpClient) {}

  getVideosByCategory(id, categoryName, page = 1) {
    return this.apiService.get('/get/censor/videos/by/thcategory/' + id + '/' + categoryName + '?page=' + page);
  
  }

  
  getVideoDetails(videoId) {
    return this.apiService.get('/get/thvideo/details/' + videoId);
  }
}
