import { Injectable } from '@angular/core';
import {ApiService} from './api.service';

@Injectable({
  providedIn: 'root'
})
export class AboutService {

  constructor(private apiService: ApiService) { }

  getMediaList() {
    return this.apiService.get('/get/media/list');
  }

  getPartners() {
    return this.apiService.get('/get/partners/list');
  }

  getTeams() {
    return this.apiService.get('/get/teams/list');
  }

  getTestimonials() {
    return this.apiService.get('/get/testimonials/list');
  }

  getAboutText() {
    return this.apiService.get('/get/content/about-us');
  }

  getPrivacyText() {
    return this.apiService.get('/get/content/privacy');
  }

  getTermsText() {
    return this.apiService.get('/get/content/terms');
  }
}
