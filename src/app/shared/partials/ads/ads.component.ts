import {Component, OnInit} from '@angular/core';
declare let adsbygoogle: any[];

@Component({
  selector: 'app-ads',
  templateUrl: './ads.component.html',
  styleUrls: ['./ads.component.css']
})
export class AdsComponent implements OnInit {

  constructor() {}

  ngOnInit(): void {
    setTimeout(() => {
      try {
        (adsbygoogle = adsbygoogle || []).push({});
      } catch (e) {}
    }, 100);
  }
}
