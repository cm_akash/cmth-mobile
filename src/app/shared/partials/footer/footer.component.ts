import { Component, OnInit } from '@angular/core';
import {HomeService} from '../../services/home.service';
import {VideoCategoryModel} from '../../models/video-category.model';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  panelOpenState = true;
  categories: VideoCategoryModel[] = [];
  constructor(private homeService: HomeService) { }

  ngOnInit() {
    this.homeService.getAllCategories().subscribe(res => {
      this.categories = res.data;
    });
  }

}
