export interface VideoModel {
  id: number;
  title: string;
  description: string;
  tags: string;
  thumbnail_url: string;
  video_url: string;
  status: string;
  video_categories_id: number;
  duration: string;
  language: string;
  unique_filename: string;
  dm_video_id: string;
  created_at: string;
  updated_at: string;
  deleted_at: string;
  category_name: string;
}
