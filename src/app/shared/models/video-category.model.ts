export interface VideoCategoryModel {
  id: number;
  category_name: string;
  category_description: string;
  dm_playlist_id: string;
  status: string;
  trending: string;
  created_at: string;
  updated_at: string;
  deleted_at: string;
  color: string;
}
