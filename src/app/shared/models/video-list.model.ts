import {VideoModel} from './video.model';

export interface VideoListModel {
  current_page: number;
  data: VideoModel[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  path: string;
  per_page: number;
  prev_page_url: string;
  to: number;
  total: number;
}
