import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './partials/header/header.component';
import { FooterComponent } from './partials/footer/footer.component';
import {MatButtonModule, MatCardModule, MatChipsModule, MatDividerModule, MatIconModule, MatToolbarModule} from '@angular/material';
import {RouterModule} from '@angular/router';
import { SlugifyPipe } from './pipes/slugify.pipe';
import { SafeurlPipe } from './pipes/safeurl.pipe';
import { AdsComponent } from './partials/ads/ads.component';
import { LazyImgComponent } from './partials/lazy-img/lazy-img.component';
import { ConnectionSnackbarComponent } from './partials/connection-snackbar/connection-snackbar.component';

@NgModule({
  declarations: [HeaderComponent, FooterComponent, SlugifyPipe, SafeurlPipe, AdsComponent, LazyImgComponent, ConnectionSnackbarComponent],
  imports: [
    CommonModule,
    RouterModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    MatChipsModule
  ],
  exports: [HeaderComponent, FooterComponent, SlugifyPipe, SafeurlPipe, LazyImgComponent, ConnectionSnackbarComponent]
})
export class SharedModule { }
