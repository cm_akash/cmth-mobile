import { Component, OnInit } from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {AboutService} from '../../shared/services/about.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.css']
})
export class PartnerComponent implements OnInit {

  title = 'Our Partner Network | Comedy Munch';
  keywords = '';
  description = '';
  partnerList: any;
  constructor(private aboutService: AboutService,
              private titleService: Title,
              private metaService: Meta,
              private spinner: NgxSpinnerService) {
    titleService.setTitle(this.title);
    metaService.addTag({name: 'keywords', content: this.keywords});
    metaService.addTag({name: 'description', content: this.description});
  }

  ngOnInit() {
    this.spinner.show();
    this.getPartnerList();
  }

  getPartnerList() {
    this.aboutService.getPartners().subscribe(response => {
      this.partnerList = response.data;
      this.spinner.hide();
    });
  }

}
