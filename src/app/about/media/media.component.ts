import { Component, OnInit } from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {AboutService} from '../../shared/services/about.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.css']
})
export class MediaComponent implements OnInit {

  mediaList: any;
  title = 'What Media says about Comedy Munch!';
  keywords = '';
  description = 'And We got featured! We really appriciate the love and support from our media partners. Big thanks to YourStory, Business Standard, Afaqs, Business today, Enterprenure, whatsup life and more. - ComedyMunch';
  constructor(private aboutService: AboutService,
              private titleService: Title,
              private metaService: Meta,
              private spinner: NgxSpinnerService) {
    titleService.setTitle(this.title);
    metaService.addTag({name: 'keywords', content: this.keywords});
    metaService.addTag({name: 'description', content: this.description});
  }

  ngOnInit() {
    this.spinner.show();
    this.getMediaList();
  }

  getMediaList() {
    this.aboutService.getMediaList().subscribe(response => {
      this.mediaList = response.data;
      this.spinner.hide();
    });
  }

}
