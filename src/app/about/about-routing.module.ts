import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MediaComponent} from './media/media.component';
import {PartnerComponent} from './partner/partner.component';
import {PrivacyComponent} from './privacy/privacy.component';
import {TermsComponent} from './terms/terms.component';
import {TeamsComponent} from './teams/teams.component';
import {AboutUsComponent} from './about-us/about-us.component';
import {IndexComponent} from './index/index.component';

const routes: Routes = [
  {path: '', component: IndexComponent, children: [
      {path: 'us', component: AboutUsComponent},
      {path: 'team', component: TeamsComponent},
      {path: 'media', component: MediaComponent},
      {path: 'partners', component: PartnerComponent},
      {path: 'privacy-policy', component: PrivacyComponent},
      {path: 'terms-and-conditions', component: TermsComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AboutRoutingModule { }
