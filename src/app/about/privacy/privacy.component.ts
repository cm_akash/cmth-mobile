import { Component, OnInit } from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {AboutService} from '../../shared/services/about.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.css']
})
export class PrivacyComponent implements OnInit {

  title = 'Privacy Policy | Comedy Munch';
  keywords = '';
  description = '';
  privacyText: any;
  constructor(private aboutService: AboutService,
              private titleService: Title,
              private metaService: Meta,
              private spinner: NgxSpinnerService) {
    titleService.setTitle(this.title);
    metaService.addTag({name: 'keywords', content: this.keywords});
    metaService.addTag({name: 'description', content: this.description});
  }

  ngOnInit() {
    this.spinner.show();
    this.getPrivacyText();
  }

  getPrivacyText() {
    this.aboutService.getPrivacyText().subscribe(response => {
      this.privacyText = response.data;
      this.spinner.hide();
    });
  }

}
