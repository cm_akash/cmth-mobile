import { Component, OnInit } from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {AboutService} from '../../shared/services/about.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {

  teamList: any;
  title = 'Our Team | ComedyMunch';
  keywords = '';
  description = 'We wouldn\'t be a \'Munch\' without our amazing team. Know More About Our Team.';
  constructor(private aboutService: AboutService,
              private titleService: Title,
              private metaService: Meta,
              private spinner: NgxSpinnerService) {
    titleService.setTitle(this.title);
    metaService.addTag({name: 'keywords', content: this.keywords});
    metaService.addTag({name: 'description', content: this.description});
  }

  ngOnInit() {
    this.spinner.show();
    this.getTeamList();
  }

  getTeamList() {
    this.aboutService.getTeams().subscribe(response => {
      this.teamList = response.data;
      this.spinner.hide();
    });
  }

}
