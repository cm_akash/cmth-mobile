import { Component, OnInit } from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {AboutService} from '../../shared/services/about.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {

  title = 'Terms & Condition (Open Mic) | Comedy Munch';
  keywords = '';
  description = '';
  termsText: any;
  constructor(private aboutService: AboutService,
              private titleService: Title,
              private metaService: Meta,
              private spinner: NgxSpinnerService) {
    titleService.setTitle(this.title);
    metaService.addTag({name: 'keywords', content: this.keywords});
    metaService.addTag({name: 'description', content: this.description});}


  ngOnInit() {
    this.spinner.show();
    this.getTermsText();
  }

  getTermsText() {
    this.aboutService.getTermsText().subscribe(response => {
      this.termsText = response.data;
      this.spinner.hide();
    });
  }

}
