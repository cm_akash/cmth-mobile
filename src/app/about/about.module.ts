import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutUsComponent } from './about-us/about-us.component';
import { MediaComponent } from './media/media.component';
import { PartnerComponent } from './partner/partner.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TeamsComponent } from './teams/teams.component';
import { TermsComponent } from './terms/terms.component';
import {AboutRoutingModule} from './about-routing.module';
import { IndexComponent } from './index/index.component';

@NgModule({
  declarations: [AboutUsComponent, MediaComponent, PartnerComponent, PrivacyComponent, TeamsComponent, TermsComponent, IndexComponent],
  imports: [
    CommonModule,
    AboutRoutingModule
  ]
})
export class AboutModule { }
