import { Component, OnInit } from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {AboutService} from '../../shared/services/about.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {

  aboutText: any;
  title = 'About Us | Comedy Munch';
  keywords = '';
  description = '';
  constructor(private aboutService: AboutService,
              private titleService: Title,
              private metaService: Meta,
              private spinner: NgxSpinnerService) {
    titleService.setTitle(this.title);
    metaService.addTag({name: 'keywords', content: this.keywords});
    metaService.addTag({name: 'description', content: this.description});
  }

  ngOnInit() {
    this.spinner.show();
    this.getAboutText();
  }

  getAboutText() {
    this.aboutService.getAboutText().subscribe(response => {
      this.aboutText = response.data;
      this.spinner.hide();
    });
  }

}
