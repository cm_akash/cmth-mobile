import { Component, OnInit } from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {ActivatedRoute} from '@angular/router';
import {TitleCasePipe} from '@angular/common';
import {VideoService} from '../../shared/services/video.service';
import {VideoListModel} from '../../shared/models/video-list.model';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-list-by-category',
  templateUrl: './list-by-category.component.html',
  styleUrls: ['./list-by-category.component.css'],
  providers: [TitleCasePipe]
})
export class ListByCategoryComponent implements OnInit {
  title = 'Comedy Munch - Watch Best Comedy Videos';
  keywords = 'Comedian for Corporate Events in Delhi, best corporate comedians ,corporate comedian india, corprate comedian in india, best stand up comedians in delhi, delhi based stand up comedian, stand up comedians in delhi, book motivational speakers, Artist for private party, Anchor in Delhi';
  description = 'Comedy Munch helps you find the best artist for your private or corporate events. As we are connected and work closely with comedians, musicians, poets, anchors, speakers including established and young and promising talent to bring happiness in your life.  Call us on +91 9205170171.';
  videos: VideoListModel = {
    current_page: null,
    data: [],
    first_page_url: null,
    from: null,
    last_page: null,
    last_page_url: null,
    path: null,
    per_page: null,
    prev_page_url: null,
    to: null,
    total: null
  };
  categoryName: string;
  categoryId: string;
  page = 1;
  constructor(private metaService: Meta,
              private titleService: Title,
              private route: ActivatedRoute,
              private videoService: VideoService,
              private titleCase: TitleCasePipe,
              private spinner: NgxSpinnerService) {
    titleService.setTitle(this.title);
    metaService.addTag({name: 'keywords', content: this.keywords});
    metaService.addTag({name: 'description', content: this.description});
    this.categoryName = this.convertParamFromUrlToCategory(this.route.snapshot.paramMap.get('category'));
    this.categoryId = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.spinner.show();
    this.getVideoByCategory();
  }

  convertParamFromUrlToCategory(originalParam) {
    return this.titleCase.transform(originalParam.replace('-', ' '));
  }

  getVideoByCategory() {
    this.videoService.getVideosByCategory(this.categoryId, this.categoryName, this.page).subscribe(response => {
      this.videos.current_page = response.current_page;
      this.videos.data.push(...response.data);
      this.videos.first_page_url = response.first_page_url;
      this.videos.from = response.from;
      this.videos.last_page = response.last_page;
      this.videos.last_page_url = response.last_page_url;
      this.videos.path = response.path;
      this.videos.from = response.from;
      this.videos.per_page = response.per_page;
      this.videos.prev_page_url = response.prev_page_url;
      this.videos.to = response.to;
      this.videos.total = response.total;
      this.spinner.hide();
    });
  }

  onScroll() {
    this.page = this.page + 1;
    this.getVideoByCategory();
  }

}
