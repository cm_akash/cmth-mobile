import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoriesComponent } from './categories/categories.component';
import { ListByCategoryComponent } from './list-by-category/list-by-category.component';
import { PlayerComponent } from './player/player.component';
import {VideosRoutingModule} from './videos-routing.module';
import { IndexComponent } from './index/index.component';
import {MatButtonModule, MatCardModule, MatChipsModule} from '@angular/material';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [CategoriesComponent, ListByCategoryComponent, PlayerComponent, IndexComponent],
  imports: [
    CommonModule,
    VideosRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatChipsModule,
    InfiniteScrollModule,
    SharedModule
  ]
})
export class VideosModule {}
