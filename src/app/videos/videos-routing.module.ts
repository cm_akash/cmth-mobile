import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListByCategoryComponent} from './list-by-category/list-by-category.component';
import {PlayerComponent} from './player/player.component';
import {IndexComponent} from './index/index.component';

const routes: Routes = [
  {path: '', component: IndexComponent, children: [
      {path: 'list/:id/:category', component: ListByCategoryComponent},
      {path: 'player/:videoId/:playlistId/:playlistName', component: PlayerComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VideosRoutingModule { }
