import {Component, OnInit} from '@angular/core';
import {HomeService} from '../../shared/services/home.service';
import {Meta, Title} from '@angular/platform-browser';
import {VideoService} from '../../shared/services/video.service';
import {TitleCasePipe} from '@angular/common';
import {VideoListModel} from '../../shared/models/video-list.model';
import {ActivatedRoute} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css'],
  providers: [TitleCasePipe]
})
export class PlayerComponent implements OnInit {
  title = 'Comedy Munch - Watch Best Comedy Videos';
  keywords = 'Comedian for Corporate Events in Delhi, best corporate comedians ,corporate comedian india, corprate comedian in india, best stand up comedians in delhi, delhi based stand up comedian, stand up comedians in delhi, book motivational speakers, Artist for private party, Anchor in Delhi';
  description = 'Comedy Munch helps you find the best artist for your private or corporate events. As we are connected and work closely with comedians, musicians, poets, anchors, speakers including established and young and promising talent to bring happiness in your life.  Call us on +91 9205170171.';
  similarVideos: VideoListModel = {
    current_page: null,
    data: [],
    first_page_url: null,
    from: null,
    last_page: null,
    last_page_url: null,
    path: null,
    per_page: null,
    prev_page_url: null,
    to: null,
    total: null
  };
  categoryName: string;
  categoryId: string;
  videoId: string;
  page = 1;
  videoDetails: any;
  videoUrl: string;
  constructor(private homeService: HomeService,
              private metaService: Meta,
              private titleService: Title,
              private route: ActivatedRoute,
              private videoService: VideoService,
              private titleCase: TitleCasePipe,
              private spinner: NgxSpinnerService) {
    titleService.setTitle(this.title);
    metaService.addTag({name: 'keywords', content: this.keywords});
    metaService.addTag({name: 'description', content: this.description});
    this.route.params.subscribe(params => {
      this.categoryId = params.playlistId;
      this.categoryName = this.convertParamFromUrlToCategory(params.playlistName);
      this.videoId = params.videoId;
      this.videoUrl = '';
      this.getVideoDetails();
    });
  }

  ngOnInit() {
    this.spinner.show();
    this.getVideoDetails();
    this.getVideoByCategory();
  }

  getVideoDetails() {
    this.videoService.getVideoDetails(this.videoId).subscribe(response => {
      this.videoDetails = response.data;
      this.videoDetails.views_total += Math.floor((Math.random() * 99999) + 10000);
      this.videoUrl = this.videoDetails.embed_url + '?autoplay=true&ui-logo=false&sharing-enable=false&queue-autoplay-next=false&queue-enable=false&playsinline=true';
      this.spinner.hide();
    });
  }

  convertParamFromUrlToCategory(originalParam) {
    return this.titleCase.transform(originalParam.replace('-', ' '));
  }

  getVideoByCategory() {
    this.videoService.getVideosByCategory(this.categoryId, this.categoryName, this.page).subscribe(response => {
      this.similarVideos.current_page = response.current_page;
      this.similarVideos.data.push(...response.data);
      this.similarVideos.first_page_url = response.first_page_url;
      this.similarVideos.from = response.from;
      this.similarVideos.last_page = response.last_page;
      this.similarVideos.last_page_url = response.last_page_url;
      this.similarVideos.path = response.path;
      this.similarVideos.from = response.from;
      this.similarVideos.per_page = response.per_page;
      this.similarVideos.prev_page_url = response.prev_page_url;
      this.similarVideos.to = response.to;
      this.similarVideos.total = response.total;
    });
  }

  onScroll() {
    this.page = this.page + 1;
    this.getVideoByCategory();
  }

}
