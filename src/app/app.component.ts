import {ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import {NavigationEnd, Router} from '@angular/router';
import {ConnectionService} from 'ng-connection-service';
import {ConnectionSnackbarComponent} from './shared/partials/connection-snackbar/connection-snackbar.component';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {

  title = 'cm-mobile-web';
  mobileQuery: MediaQueryList;
  sideNavMenuItems = [
    {name: 'Home', link: '/'},
    {name: 'About Us', link: '/about/us'},
    {name: 'Media', link: '/about/media'},
    {name: 'Partners', link: '/about/partners'},
    {name: 'Team', link: '/about/team'},
    {name: 'Privacy Policy', link: '/about/privacy-policy'},
    {name: 'Terms and Condition', link: '/about/terms-and-conditions'}
  ];
  isConnected = true;

  // shouldRun = [/(^|\.)plnkr\.co$/, /(^|\.)stackblitz\.io$/].some(h => h.test(window.location.host));

  mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef,
              media: MediaMatcher,
              private router: Router,
              private connectionService: ConnectionService,
              private snackBar: MatSnackBar) {
    // this.checkConnectionStatus();
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    if (!this.mobileQuery.matches) {
      this.snackBar.openFromComponent(ConnectionSnackbarComponent, {
        duration: 5000,
        verticalPosition: 'top',
        horizontalPosition: 'center',
        data: {message: 'For Best Experience, Please Switch to a Mobile Device.'}
      });
    }
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this.mobileQueryListener);
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        window.scrollTo(0, 0);
      }
    });
  }

  checkConnectionStatus() {
    this.connectionService.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
      if (this.isConnected) {
        this.snackBar.openFromComponent(ConnectionSnackbarComponent, {
          duration: 2000,
          verticalPosition: 'top',
          horizontalPosition: 'center',
          data: {message: 'You are ONLINE!'}
        });
      } else {
        this.snackBar.openFromComponent(ConnectionSnackbarComponent, {
          duration: 2000,
          verticalPosition: 'top',
          horizontalPosition: 'center',
          data: {message: 'You are OFFLINE!'}
        });
      }
    });
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this.mobileQueryListener);
  }
}
