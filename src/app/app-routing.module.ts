import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';

const routes: Routes = [
  //{path: '', component: HomeComponent},
  {path: ' ', redirectTo: 'videos/list/4/viral'},
  {path: 'auth', loadChildren: './auth/auth.module#AuthModule'},
  {path: 'videos', loadChildren: './videos/videos.module#VideosModule'},
  {path: 'about', loadChildren: './about/about.module#AboutModule'},
  {path: '**', redirectTo: 'videos/list/4/viral'},
  {path: '**/', redirectTo: 'videos/list/4/viral'},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
