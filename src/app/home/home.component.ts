import { Component, OnInit } from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {HomeService} from '../shared/services/home.service';
import {VideoCategoryModel} from '../shared/models/video-category.model';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  title = 'Comedy Munch - Watch Best Comedy Videos';
  keywords = 'Comedian for Corporate Events in Delhi, best corporate comedians ,corporate comedian india, corprate comedian in india, best stand up comedians in delhi, delhi based stand up comedian, stand up comedians in delhi, book motivational speakers, Artist for private party, Anchor in Delhi';
  description = 'Comedy Munch helps you find the best artist for your private or corporate events. As we are connected and work closely with comedians, musicians, poets, anchors, speakers including established and young and promising talent to bring happiness in your life.  Call us on +91 9205170171.';
  categories: VideoCategoryModel[] = [];
  materialColorsArr = ['#f44336', '#e91e63', '#651fff', '#9c27b0', '#03a9f4', '#ff5722', '#5d4037', '#4caf50', '#37474f', '#3f51b5', '#00695c'];
  constructor(private homeService: HomeService,
              private metaService: Meta,
              private titleService: Title,
              private spinner: NgxSpinnerService) {
    titleService.setTitle(this.title);
    metaService.addTag({name: 'keywords', content: this.keywords});
    metaService.addTag({name: 'description', content: this.description});
  }

  ngOnInit() {
    this.spinner.show();
    this.getVideoCategories();
  }

  getVideoCategories() {
    this.homeService.getAllCategories().subscribe(response => {
      this.categories = response.data;
      this.categories.forEach((cat, i) => {
        cat.color = this.materialColorsArr[i];
      });
      this.spinner.hide();
    });
  }
}
